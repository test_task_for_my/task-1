<?php

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<title>Тестовое задание 1</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<? if(empty($list_prod)): ?>
				<p class="alert alert-info">Пустой список продуктов</p>
			<? else: ?>
				<table class="table table-hover table-bordered table-responsive">
					<thead>
					<tr>
						<th>Форма</th>
						<th>Размер</th>
						<th>Цвет</th>
						<th>Группа</th>
						<th>Название</th>
					</tr>
					</thead>
					<tbody>
					<? foreach($list_prod as $prod): ?>
						<tr class="">
							<td class=""><?=$prod['form']?>: <?=$prod['form_name']?></td>
							<td class=""><?=$prod['size']?></td>
							<td class=""><?=$prod['color']?>: <?=$prod['color_name']?></td>
							<td class=""><?=$prod['group']?></td>
							<td class=""><?=$prod['name_in']?></td>
						</tr>
					<? endforeach ?>
					</tbody>
				</table>
			<? endif ?>
		</div>
	</div>
</div>

<script type="text/javascript" src="/vendor/libra/jquery-assets/public/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
