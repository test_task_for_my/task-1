<?php
require __DIR__.'/config.php';
require __DIR__.'/vendor/autoload.php';

$sql = "SELECT
	p.*,
	pf.value as form_name,
	pc.value as color_name
FROM products p
	INNER JOIN param_color pc ON pc.id = p.color
	INNER JOIN param_form pf ON pf.id = p.form
 ORDER BY form_name ASC,p.size DESC, color_name DESC, p.`group` DESC";
$prep =DBClient::getInstance()->pdo->prepare($sql);
$prep->execute();
$list_prod = $prep->fetchAll();


require __DIR__.'/tmpl/page.php';
